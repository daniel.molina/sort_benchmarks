#ifndef _SORTS_H
#define _SORTS_H

void burbuja(int *v, int n);
void insercion(int *v, int N);
void seleccion(int *v, int N);
void quicksort(int *v, int N);
void mergesort(int *v, int N);

#endif
