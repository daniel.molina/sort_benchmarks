#include "common.h"
#include <cassert>
#include <algorithm>
#include <iterator>
#include "random.hpp"

using Random = effolkronium::random_static;
using namespace std;

using namespace std::chrono;

void swap(int &value1, int &value2) {
  auto tmp = value2;
  value2 = value1;
  value1 = tmp;
}

int *init_vector(int N) {
  assert(N > 0);
  int *v = new int[N];

  if (v == nullptr) {
    return nullptr;
  }

  for (int i = 0; i < N; i++) {
    v[i] = Random::get(1, 2*N);
  }

  return v;
}

void free_vector(int *&vector) {
  delete[] vector;
  vector = nullptr;
}

int *copy(int *vector, int N) {
  int *result = new int[N];
  assert(result != nullptr);
  copy(vector, vector+N, result);
  return result;
}


steady_clock::time_point begin_benchmark() {
  return chrono::steady_clock::now();
}

steady_clock::time_point end_benchmark() {
  return begin_benchmark();
}

int get_milliseconds(const steady_clock::time_point &begin, const steady_clock::time_point &end) {
  auto diff_millisecond = chrono::duration_cast<chrono::milliseconds>(end-begin);
  return diff_millisecond.count();
}

void shuffle(int *v, int N) {
  Random::shuffle(v, v+N);
}
