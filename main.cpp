#include <iostream>
#include "common.h"
#include "sorts.h"

using namespace std;

typedef void functionSort(int*, int);

int measureSort(functionSort sort, int *v, int N) {
  int *aux = copy(v, N);
  auto begin_time = begin_benchmark();
  sort(aux, N);
  auto end_time = end_benchmark();
  free_vector(aux);
  return get_milliseconds(begin_time, end_time);
}

int measureSortTimes(functionSort sort, int *v, int N, int times) {
  int sum = 0;

  for (int i = 0; i < times; i++) {
    sum += measureSort(sort, v, N);
    shuffle(v, N);
  }

  return sum/times;
}

void print_by_random(int *v, int N) {
  string prefix = "Random";
  int times = 10;

  int time_burbuja = measureSortTimes(burbuja, v, N, times);
  cout <<prefix <<"::Burbuja: " <<time_burbuja <<endl;
  int time_insert = measureSortTimes(insercion, v, N, times);
  cout << prefix << "::Inserción: " <<time_insert <<endl;
  int time_select = measureSortTimes(seleccion, v, N, times);
  cout << prefix << "::Selección: " <<time_select <<endl;
  int time_merge = measureSortTimes(mergesort, v, N, times);
  cout << prefix << "::Mergesort: " <<time_merge <<endl;
  int time_quick = measureSort(quicksort, v, N);
  cout << prefix << "::Quicksort: " <<time_quick <<endl;
}

void print_by_input(const string &prefix, int *v, int N) {
  if (prefix == "Random") {
    print_by_random(v, N);
  }
  else {
    int time_burbuja = measureSort(burbuja, v, N);
    cout <<prefix <<"::Burbuja: " <<time_burbuja <<endl;
    int time_insert = measureSort(insercion, v, N);
    cout << prefix << "::Inserción: " <<time_insert <<endl;
    int time_select = measureSort(seleccion, v, N);
    cout << prefix << "::Selección: " <<time_select <<endl;
    int time_merge = measureSort(mergesort, v, N);
    cout << prefix << "::Mergesort: " <<time_merge <<endl;
    int time_quick = measureSort(quicksort, v, N);
    cout << prefix << "::Quicksort: " <<time_quick <<endl;
  }
}

void reverse(int *v, int N) {
  int half = N/2;

  for (int i = 0; i < half; i++) {
    swap(v[i], v[N-1-i]);
  }
}

int main(int argc, char *argv[])
{
  int N=10000;
  int *v;

  if (argc > 1) {
    N = atoi(argv[1]);
  }

  v = init_vector(N);

  if (v == nullptr) {
    cerr <<"Error, vector not initialised" <<endl;
    return 1;
  }

  print_by_random(v, N);
  // Sort the solution
  insercion(v, N);
  print_by_input("Sorted", v, N);
  // Reverse
  reverse(v, N);
  print_by_input("Reversed", v, N);

  free_vector(v);
  return 0;
}

