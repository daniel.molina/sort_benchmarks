#include "common.h"

void insercion(int *v, int N) {
  int current;

  for (int i = 1; i < N; i++) {
    auto elem = v[i];
    current = i-1;

    while (current >= 0 && v[current] > elem) {
      v[current+1] = v[current];
      current--;
    }

    v[current+1] = elem;
  }
}
