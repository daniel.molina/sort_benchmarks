#ifndef _COMMON_H

#define _COMMON_H

#include <chrono>
using namespace std::chrono;


/**
 * Swap two number.
 * @param v1 first value to swap.
 * @param v2 first value to swap.
 */
void swap(int &value1, int &value2);

int *copy(int *vector1, int N);

/**
 * Shuffle the vector.
 *
 * @param vector to shuffle.
 * @param N size of the vector.
 */
void shuffle(int *vector, int N);

/**
 * Create and init a vector of integers with random values between [1,2N].
 * @param N size of the parameter.
 */
int *init_vector(int N);

/**
 * Free vector reserved by init_vector.
 * @param vector to remove
 * @see init_vector
 */
void free_vector(int *&vector);

/**
 * Start measuring a function
 */
steady_clock::time_point begin_benchmark();

/**
 * Start measuring a function
 */
steady_clock::time_point end_benchmark();

/**
 * Returns the time in milliseconds between begin and start time.
 * @param begin time obtained by begin_benchmark before the function call.
 * @param end time obtained by end_benchmark after the function call.
 * @return time in milliseconds
 */
int get_milliseconds(const steady_clock::time_point &begin, const steady_clock::time_point &end);

#endif // _COMMON_H
