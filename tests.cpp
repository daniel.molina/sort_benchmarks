#include "catch.hpp"
#include "common.h"
#include "sorts.h"
#include "random.hpp"
#include "search.h"
#include <iostream>

using Random = effolkronium::random_static;

bool is_sorted(int *v, int N) {
  bool sorted = true;

  for (int i=1; i < N && sorted; i++) {
    if (v[i-1] > v[i]) {
      sorted = false;
    }
  }
  return sorted;
}

/**
 * Compare that two vectors have the same elements
 * @param vector1 to compare.
 * @param vector2 to compare.
 */
bool equals_elems(int *vector1, int *vector2, int N) {
  int * aux = copy(vector2, N);
  bool foundall = true;

  for (int i = 0; i < N && foundall; i++) {
    auto elem = vector1[i];
    foundall = false;

    for (int j = 0; j < N && !foundall; j++) {
      if (elem == aux[j]) {
        aux[j] = -1;
        foundall = true;
      }
    }

  }

  delete[] aux;
  return foundall;
}

TEST_CASE( "Search is working", "[search]") {
  const int N = 30;
  int v[N];

  for (int i = 0; i < N; i++) {
    v[i] = i*2;
  }

  const int max=40;
  REQUIRE(binarySearch(v, N, N*3)==-1);

  for (int i = 0; i < max; i++) {
    int pos = Random::get<int>(0, N-1);
    CAPTURE(pos, v[pos], v[pos-1], v[pos+1], binarySearch(v, N, v[pos]));
    REQUIRE(binarySearch(v, N, v[pos])==pos);
  }
}

TEST_CASE( "Sorts is working", "[utils]" ) {
  auto N = GENERATE(10, 100, 1000, 3000, 5000, 7000);

  SECTION("Random is working") {
      int *v = init_vector(N);
      REQUIRE(is_sorted(v, N)==false);
      free_vector(v);
  }

  SECTION("Burbuja is working") {
    int *v = init_vector(N);
    REQUIRE(is_sorted(v, N)==false);
    int *clone = copy(v, N);
    burbuja(v, N);
    REQUIRE(is_sorted(v, N)==true);
    REQUIRE(equals_elems(v, clone, N)==true);
    free_vector(v);
  }
  SECTION("Insercion is working") {
    int *v = init_vector(N);
    REQUIRE(is_sorted(v, N)==false);
    int *clone = copy(v, N);
    insercion(v, N);
    REQUIRE(is_sorted(v, N)==true);
    REQUIRE(equals_elems(v, clone, N)==true);
    free_vector(v);
  }
  SECTION("Seleccion is working") {
    int *v = init_vector(N);
    REQUIRE(is_sorted(v, N)==false);
    int *clone = copy(v, N);
    seleccion(v, N);
    REQUIRE(is_sorted(v, N)==true);
    REQUIRE(equals_elems(v, clone, N)==true);
    free_vector(v);
  }

  SECTION("Quicksort is working") {
    int *v = init_vector(N);
    REQUIRE(is_sorted(v, N)==false);
    int *clone = copy(v, N);
    quicksort(v, N);
    REQUIRE(is_sorted(v, N)==true);
    REQUIRE(equals_elems(v, clone, N)==true);
    free_vector(v);
  }

  SECTION("Mergesort is working") {
    int *v = init_vector(N);
    REQUIRE(is_sorted(v, N)==false);
    int *clone = copy(v, N);
    mergesort(v, N);
    REQUIRE(is_sorted(v, N)==true);
    REQUIRE(equals_elems(v, clone, N)==true);
    free_vector(v);
  }



}
