#include "common.h"

static void quicksort(int *v, int low, int hi);
static int partition(int *v, int low, int hi);

void quicksort(int *v, int N) {
  quicksort(v, 0, N-1);
}

static void quicksort(int *v, int low, int hi) {
  if (low >= hi) {
    return;
  }

  int p = partition(v, low, hi);
  quicksort(v, low, p-1);
  quicksort(v, p+1, hi);
}

int partition(int * v, int start, int end) {
  int pivot = v[start];
  int i = start+1;  int j = end;

  while (i <= j) {
    while (i <= j && v[i] < pivot) {
      i++;
    }

    while (i <= j && v[j] > pivot) {
      j--;
    }

    if (i <= j) {
      swap(v[i++], v[j--]);
    }
  }

  if (start < j) {
    swap(v[start], v[j]);
  }

  return j;
}
