#include <iostream>

using namespace std;

static void mergesort(int *v, int low, int hi);
static void merge(int *v1, int max1, int *v2, int max2, int *output);

void mergesort(int *v, int N) {
  mergesort(v, 0, N-1);
}

void mergesort(int *v, int low, int high) {
  int size = high-low+1;

  if (low >= high) {
    return;
  }

  int half = (low+high)/2;
  mergesort(v, low, half);
  mergesort(v, half+1, high);
  int *aux = new int[size];
  merge(v+low, half-low+1, v+half+1, high-half, aux);
  copy(aux, aux+size, v+low);
  delete[] aux;
}

static void merge(int *v1, int max1, int *v2, int max2, int *output) {
  int pos1, pos2;
  int val1, val2;
  int max = 0;

  pos1 = 0;
  pos2 = 0;
  val1 = v1[pos1];
  val2 = v2[pos2];

  while (pos1 < max1 && pos2 < max2) {

    if (val1 < val2) {
      output[max] = val1;
      max += 1;
      pos1 += 1;
      val1 = v1[pos1];
    }
    else {
      output[max] = val2;
      max += 1;
      pos2 += 1;
      val2 = v2[pos2];
    }
  }

  if (pos1 < max1) {
    output[max] = val1;

    for (int i = 1; i < max1-pos1; i++) {
      output[max+i] = v1[pos1+i];
    }
  }
  else if (pos2 < max2) {
    output[max] = val2;

    for (int i = 1; i < max2-pos2; i++) {
      output[max+i] = v2[pos2+i];
    }
  }

}


static void merge_short(int *v1, int max1, int *v2, int max2, int *output) {
  int pos1, pos2;
  int max = 0;

  pos1 = pos2 = 0;

  while (pos1 < max1 && pos2 < max2) {
    if (v1[pos1] < v2[pos2]) {
      output[max++] = v1[pos1++];
    }
    else {
      output[max++] = v2[pos2++];
    }
  }

  if (pos1 < max1) {
    for (int i = 0; i < max1-max; i++) {
      output[max+i] = v1[pos1+i];
    }
  }
  else if (pos2 < max2) {
    for (int i = 0; i < max2-pos2; i++) {
      output[max+i] = v2[pos2+i];
    }
  }

}
