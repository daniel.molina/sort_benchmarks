#include "common.h"

int seleccion(int *v, int N) {
  for (int i = 0; i < N; i++) {
    int iMin = i;

    for (int j = i+1; j < N; j++) {
      if (v[j] < v[iMin]) {
        iMin = j;
      }
    }

    if (iMin != i) {
      swap(v[iMin], v[i]);
    }
  }

}
