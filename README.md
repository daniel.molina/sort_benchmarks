# Small sort benchmark in C++

This repository contains several well-known sort methods to do a small 
introduction in my programming Lectures (in Algorithms). 

## Sort functions 

The list of implemented functions are:

- [Bubble Sort](https://en.wikipedia.org/wiki/Bubble_sort).
- [Insertion Sort](https://en.wikipedia.org/wiki/Insertion_sort). 
- [Selection Sort](https://en.wikipedia.org/wiki/Selection_sort).

## Testing

To be sure that all sort functions are working, I have use testing with
[Catch2](https://github.com/catchorg/Catch2), in order to be sure that all sort
functions are right.

## Compiling

Compiling is very simple, using CMake, you only have to do:

```sh
cmake .
make
```

## Benchmarking

The benchmark is carried out in the different following cases:

1. Measuring the time from a random vector. Because it could differ based on the
   initial random vector, it is carried out ten times, and the mean is shown
   (mean case).
   
2. Sorted: Measured from a sorted vector (best case). 

3. Reversed: Measured from a reversed sorted vector (worst case). 

## Additional sources

The source code is completely self-contained, but it includes the following libraries:

- [https://github.com/effolkronium/random](https://github.com/effolkronium/random): 
  Because the random generators in C++ is not very intuitive. 
  
- [Catch2](https://github.com/catchorg/Catch2): Because it is a wonderful testing library.
