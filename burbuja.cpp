#include "common.h"

void burbuja(int *v, int N) {
  bool haschanged = true;

  while (haschanged) {
      haschanged = false;

      for (int i = 1; i < N; i++) {
        if (v[i-1] > v[i]) {
          swap(v[i-1], v[i]);
          haschanged = true;
        }
      }
  }
}
